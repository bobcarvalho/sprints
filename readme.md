## **Hello world!** 

- Índice referente ao primeiro challenge proposto pelo Scrum Master (**SPRINT 1**)

**Dia 1:** [**Onboard**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia1.md)

**Dia 2:** [**Planning sprint 1**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia2.md)

**Dia 3:** [**Scrum, papéis e responsabilidades**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia3.md)

**Dia 4:** [**Fundamentos de testes de software**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia4.md)

**Dia 5:** [**Fundamentos de testes de software (back-end)**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia5.md)

**Dia 6:** [**Myers e o princípio de Pareto**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia6.md)

**Dia 7:** [**Introdução a Java**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia7.java)

**Dia 8:** [**Java part II**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia8.java)

**Dia 9:** [**CyberSecurity**](https://gitlab.com/bobcarvalho/sprints/-/blob/master/daily_sprints/dia9.md)

![compass](https://apn-portal.my.salesforce.com/servlet/servlet.ImageServer?id=0158a000005eHP3AAM&oid=00DE0000000c48tMAA)
